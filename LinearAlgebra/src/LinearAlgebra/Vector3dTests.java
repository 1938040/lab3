package LinearAlgebra;
//Jamin Huang 1938040
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testMakeVector3d() {
		Vector3d vector1=new LinearAlgebra.Vector3d(2,3,4);
		assertEquals(2.0,vector1.getx());
		assertEquals(3.0,vector1.gety());
		assertEquals(4.0,vector1.getz());
		
		Vector3d vector2=new LinearAlgebra.Vector3d(-1,-2,-3);
		assertEquals(-1.0,vector2.getx());
		assertEquals(-2.0,vector2.gety());
		assertEquals(-3.0,vector2.getz());
		
		Vector3d vector3=new LinearAlgebra.Vector3d(0, 0, 0);
		assertEquals(0.0,vector3.getx());
		assertEquals(0.0,vector3.gety());
		assertEquals(0.0,vector3.getz());
		//tested if there were any problems for 0,negative,positive entries
		}
	@Test
	void testMagnitude() {
		Vector3d vector1=new LinearAlgebra.Vector3d(2,3,4);
		assertEquals(Math.sqrt(29),vector1.magnitude());
	}
	@Test
	void testDotProduct() {
		Vector3d vector1=new LinearAlgebra.Vector3d(2,3,4);
		Vector3d vector2=new LinearAlgebra.Vector3d(1,2,3);
		assertEquals(20,vector1.dotProduct(vector2));
	}
	@Test
	void testAdd() {
		Vector3d vector1=new LinearAlgebra.Vector3d(2,3,4);
		Vector3d vector2=new LinearAlgebra.Vector3d(-1,-2,-2);
		Vector3d vector3=vector1.add(vector2);
		assertEquals(1,vector3.getx());
		assertEquals(1,vector3.gety());
		assertEquals(2,vector3.getz());
		
		//didn't overload assertEquals method
	}

}
