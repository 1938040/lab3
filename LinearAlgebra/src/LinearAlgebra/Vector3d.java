package LinearAlgebra;
//Jamin Huang 1938040
public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x,double y,double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	public double getx() {
		return x;
	}
	public double gety() {
		return y;
	}
	public double getz() {
		return z;
	}
	
	public double magnitude() {
		double magnitude=this.x*this.x+this.y*this.y+this.z*this.z;
		magnitude=Math.sqrt(magnitude);
		
		return magnitude;
	}
	public double dotProduct(Vector3d secondVector) {
		double dotProduct=this.x*secondVector.getx()+this.y*secondVector.gety()+this.z*secondVector.getz();
		
		return dotProduct;
	}
	public Vector3d add(Vector3d secondVector) {
		double x=this.x+secondVector.getx();
		double y=this.y+secondVector.gety();
		double z=this.z+secondVector.getz();
		LinearAlgebra.Vector3d thirdVector=new Vector3d(x,y,z);
		return thirdVector;
	}
	
}
